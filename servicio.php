<?php

require_once('./include/DB.php');

// Definimos como opciones que vamos a psar al cliente SOAP las clases que van a ser mapeadas en las peticiones al servidor
$options = array('classmap' => array('Genero' => 'Genero', 'Artista' => 'Artista', 'Pelicula' => 'Pelicula'));

// Creamos el servidor SOAP pasaándole el array de clases como opciones
$server = new SoapServer('http://sergiodaw2.16mb.com/db_remoto.wsdl', $options);
//$server = new SoapServer('http://localhost/ProyectoPeliculas/db_local.wsdl', $options);

$server->setClass('DB');

$server->handle();
