<html>
    <head>
        <title>Películas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <?php
        require_once './include/Genero.php';
        require_once './include/Artista.php';
        require_once './include/Pelicula.php';
        require_once './include/funciones.php';

        //require_once './include/DB.php';
        // Definimos como opciones que vamos a psar al cliente SOAP las clases que van a ser mapeadas en las peticiones al servidor
        $options = array('classmap' => array('Genero' => 'Genero', 'Artista' => 'Artista', 'Pelicula' => 'Pelicula'));

        // Creamos el cliente SOAP pasaándole el array de clases como opciones
        //$cliente = new SoapClient('http://sergiodaw2.16mb.com/servicio.php?wsdl', $options);
        $cliente = new SoapClient('http://localhost/ProyectoPeliculas/servicio.php?wsdl', $options);




        //Alta de generos utilizando el servicio
        $cliente->altaGeneros('Drama');
        $cliente->altaGeneros('Comedia');
        $cliente->altaGeneros('Acción');
        $cliente->altaGeneros('Aventura');
        $cliente->altaGeneros('Terror');
        $cliente->altaGeneros('Ciencia Ficción');
        $cliente->altaGeneros('Romántico');
        $cliente->altaGeneros('Musical');
        $cliente->altaGeneros('Melodrama');
        $cliente->altaGeneros('Catástrofico');
        $cliente->altaGeneros('Suspense');
        $cliente->altaGeneros('Fantasía');
        $cliente->altaGeneros('Pornográfico');

        //Alta de artistas utilizando el servicio
        $cliente->altaArtistas('Quentin', 'Tarantino');
        $cliente->altaArtistas('John', 'Travolta');
        $cliente->altaArtistas('Samuel', 'L. Jackson');
        $cliente->altaArtistas('Uma', 'Thurman');
        $cliente->altaArtistas('Harvey', 'Keitel');
        $cliente->altaArtistas('Tim', 'Roth');
        $cliente->altaArtistas('Amanda', 'Plumer');
        $cliente->altaArtistas('Maria', 'de Medeiros');
        $cliente->altaArtistas('Ving', 'Rhames');
        $cliente->altaArtistas('Rossana', 'Arquette');
        $cliente->altaArtistas('Eric', 'Stoltz');
        $cliente->altaArtistas('Christopher', 'Walken');
        $cliente->altaArtistas('Bruce', 'Willis');
        $cliente->altaArtistas('Roger', 'Avary');

        //Alta de artistas utilizando el servicio
        $cliente->altaArtistas('Frank', 'Darabont');
        $cliente->altaArtistas('Tim', 'Robbins');
        $cliente->altaArtistas('Morgan', 'Freeman');
        $cliente->altaArtistas('Bob', 'Gunton');
        $cliente->altaArtistas('James', 'Whitmore');
        $cliente->altaArtistas('Gil', 'Bellows');
        $cliente->altaArtistas('William ', 'Sadler');
        $cliente->altaArtistas('Mark', 'Rolston');
        $cliente->altaArtistas('Clancy', 'Brown');
        $cliente->altaArtistas('David', 'Proval');
        $cliente->altaArtistas('Jeffrey', 'DeMunn');
        $cliente->altaArtistas('Jude ', 'Ciccolella');
        $cliente->altaArtistas('Don', 'McManus');

        //Alta de artistas utilizando el servicio
        $cliente->altaArtistas('Ryan', 'Reinolds');
        $cliente->altaArtistas('Morena', 'Baccarin');
        $cliente->altaArtistas('T.J.', 'Miller');
        $cliente->altaArtistas('Tim', 'Miller');
        $cliente->altaArtistas('Rhett', 'Reese');
        $cliente->altaArtistas('Paul', 'Wernick');

        //Alta de peliculas utilizando el servicio
        $cliente->altaPeliculas('Pulp Fiction', '1994', '3', 'Jules y Vincent, dos asesinos a sueldo con no demasiadas luces, trabajan para el gángster Marsellus Wallace. Vincent le confiesa a Jules que Marsellus le ha pedido que cuide de Mia, su atractiva mujer. Jules le recomienda prudencia porque es muy peligroso sobrepasarse con la novia del jefe. Cuando llega la hora de trabajar, ambos deben ponerse "manos a la obra". Su misión: recuperar un misterioso maletín. ', '153');
        $cliente->altaPeliculas('Cadena perpetua', '1994', '1', 'Acusado del asesinato de su mujer, Andrew Dufresne (Tim Robbins), tras ser condenado a cadena perpetua, es enviado a la cárcel de Shawshank. Con el paso de los años conseguirá ganarse la confianza del director del centro y el respeto de sus compañeros de prisión, especialmente de Red (Morgan Freeman), el jefe de la mafia de los sobornos. ', '142');

        //Alta de los actores de la pelicula utilizando el sercicio
        $cliente->altaPeliculaActor('1', '1');
        $cliente->altaPeliculaActor('1', '2');
        $cliente->altaPeliculaActor('1', '3');
        $cliente->altaPeliculaActor('1', '4');
        $cliente->altaPeliculaActor('1', '5');
        $cliente->altaPeliculaActor('1', '6');
        $cliente->altaPeliculaActor('1', '7');
        $cliente->altaPeliculaActor('1', '8');
        $cliente->altaPeliculaActor('1', '9');
        $cliente->altaPeliculaActor('1', '10');
        $cliente->altaPeliculaActor('1', '11');
        $cliente->altaPeliculaActor('1', '12');
        $cliente->altaPeliculaActor('1', '13');
        $cliente->altaPeliculaActor('1', '14');

        //Alta de los actores de la pelicula utilizando el sercicio
        $cliente->altaPeliculaActor('2', '16');
        $cliente->altaPeliculaActor('2', '17');
        $cliente->altaPeliculaActor('2', '18');
        $cliente->altaPeliculaActor('2', '19');
        $cliente->altaPeliculaActor('2', '20');
        $cliente->altaPeliculaActor('2', '21');
        $cliente->altaPeliculaActor('2', '22');
        $cliente->altaPeliculaActor('2', '23');
        $cliente->altaPeliculaActor('2', '24');
        $cliente->altaPeliculaActor('2', '25');

        //Alta de los directores de la pelicula utilizando el sercicio
        $cliente->altaPeliculaDirector('1', '1');
        $cliente->altaPeliculaDirector('2', '15');

        //Alta de los guinistas de la pelicula utilizando el sercicio
        $cliente->altaPeliculaGuionista('1', '1');
        $cliente->altaPeliculaGuionista('2', '15');

        //Eliminacion de la relacion de un actor con una pelicula
        $cliente->bajaPeliculaActor('1', '1');

        //Eliminacion de la relacion de un director con una pelicula
        $cliente->bajaPeliculaDirector('1', '1');

        //Eliminacion de la relacion de un director con una pelicula
        $cliente->bajaPeliculaGuionista('1', '1');

        echo '<br>';

        //devuelve un array de objetos genero
        $array = corregirArrayStdClass($cliente->listaGeneros()->arrayGeneros);

        //Recorremos el array
        foreach ($array as $valor) {
            //Usamos la funcion para conseguir la descripcion del genero
            echo $valor->getTipo();

            echo '<br>';
        }

        //devuelve un array de objetos artistas
        $array = corregirArrayStdClass($cliente->listaArtistas()->arrayArtistas);

        echo '<br>';
        //Recorremos el array
        foreach ($array as $valor) {
            //Usamos la funcion para mostrar el nombre del artista
            echo $valor->mostrarNombreArtista();

            echo '<br>';
        }


        //arrays para almacenar los objetos
        $actores = array();
        $directores = array();
        $guionistas = array();
        //almacenamos un objeto genero con sus parametros
        $genero = new Genero(['id_genero' => '3', 'tipo' => 'Accion']);

        //almacenamos un objeto artisra con sus parameteros
        $artista = new Artista(['id_artista' => '28', 'nombre' => 'Ryan', 'apellidos' => 'Reinolds']);

        //lo relacionamos con actores
        $actores[] = $artista;
        //almacenamos un objeto artista con sus parameteros
        $artista = new Artista(['id_artista' => '29', 'nombre' => 'Morena', 'apellidos' => 'Baccarin']);

        //lo relacionamos con actores
        $actores[] = $artista;
        //almacenamos un objeto artista con sus parameteros
        $artista = new Artista(['id_artista' => '30', 'nombre' => 'T.J.', 'apellidos' => 'Miller']);

        //lo relacionamos con actores
        $actores[] = $artista;

        //almacenamos un objeto artista con sus parameteros
        $artista = new Artista(['id_artista' => '31', 'nombre' => 'Tim', 'apellidos' => 'Miller']);

        //lo relacionamos con directores
        $directores[] = $artista;
        //almacenamos un objeto artista con sus parameteros
        $artista = new Artista(['id_artista' => '32', 'nombre' => 'Rhett', 'apellidos' => 'Reese']);

        //lo relacionamos con guionistas
        $guionistas[] = $artista;
        //almacenamos un objeto artista con sus parameteros
        $artista = new Artista(['id_artista' => '33', 'nombre' => 'Paul', 'apellidos' => 'Wernick']);

        //lo relacionamos con guionistas
        $guionistas[] = $artista;

        //almacenamos las sipnosis en una variable
        $sinopsis = 'Basado en el anti-héroe menos convencional de la Marvel, Deadpool narra el origen de un ex-operativo de la fuerzas especiales '
                . 'llamado Wade Wilson, reconvertido a mercenario, y que tras ser sometido a un cruel experimento adquiere poderes de curación rápida, '
                . 'adoptando Wade entonces el alter ego de Deadpool. Armado con sus nuevas habilidades y un oscuro y retorcido sentido del humor, '
                . 'Deadpool intentará dar caza al hombre que casi destruye su vida. ';

        //Creamos el objeto pelicula
        $peli = new Pelicula('0', 'DeadPool', '2016', $genero, $sinopsis, '108', $directores, $guionistas, $actores);

        echo '<br>';
        echo '<br>';
        
        //Usamos la funcion para conseguir el genero y el tipo
        echo 'Genero de película: ' . $peli->getGenero()->getTipo();

        echo '<br>';
        echo '<br>';

        //Usamos el servicio para almacenar la pelicula y los artistas relacionados
        echo 'Id Pelicula Insertada: ' . $cliente->altaPelicula($peli);

        echo '<br>';

        //Usamos el servicio para eliminar una fila de la tabla pelicula
        $cliente->bajaPelicula(1);

        //Usamos el servicio para eliminar una fila de la tabla artista
        $cliente->bajaArtista(16);
        //Usamos el servicio para modificar una fila de la tabla artista
        $cliente->modificarArtista(28, 'Ramon', 'Reinols');
        //Usamos el servicio para modificar una fila de la tabla pelicula
        $cliente->modificarPelicula(3, 'Piscina de la muerte', '2015', 'Vida en la playa', '90');

        echo '<br>';

        // Recuperamos un artista haciendo uso del servicio
        $artista = $cliente->listaArtista('18');

        // Mostramos la información del artista por pantalla
        echo 'Id Artista: ' . $artista->getId_artista();
        echo '<br>';
        echo 'Nombre Artista: ' . $artista->getNombre();
        echo '<br>';
        echo 'Apellidos Artista: ' . $artista->getApellidos();
        echo '<br>';

        echo '<br>';

        // Recuperamos un género haciendo uso del servicio
        $genero = $cliente->listaGenero('4');


        // Mostramos la información del género por pantalla
        echo 'Id Genero: ' . $genero->getId_genero();
        echo '<br>';
        echo 'Tipo Genero: ' . $genero->getTipo();
        echo '<br>';
        echo '<br>';

        
        // Recuperamos una pelicula haciendo uso del servicio
        $peli = $cliente->listaPelicula('3');

        // Mostramos la información de la película por pantalla
        mostrarPelicula($peli);


        // Modificamos valores de la pelicula
        $peli->setNombre('Patatas!');
        $peli->setSinopsis('La interesante historia de las patatas hasta nuestros dias');

        // Almacenamos la película en la base de datos y recogemos el nuevo id
        $nuevo_id = $cliente->modificarPeliculaEx($peli);

        // Asignamos el nuevo id al objeto Pelicula
        $peli->setId_pelicula($nuevo_id);

        // Mostramos la información de la pelicula por pantalla
        mostrarPelicula($peli);


        // Recuperamos todas las peliculas
        $array = corregirArrayStdClass($cliente->listaPeliculas()->arrayPeliculas);

        //Recorremos el array
        foreach ($array as $valor) {
            // Mostramos la información de la pelicula
            echo mostrarPelicula($valor);
        }

        /**
         * Función que nos permite mostrar información de las películas
         * @param Pelicula $pelicula Un objeto Pelicula con información sobre una película
         */
        function mostrarPelicula(Pelicula $pelicula) {

            echo '<br>';
            echo '----------------------------------------------------------';
            echo '<br>';
            echo 'Id Pelicula: ' . $pelicula->getId_pelicula();
            echo '<br>';
            echo 'Nombre Pelicula: ' . $pelicula->getNombre();
            echo '<br>';
            echo 'Año Pelicula: ' . $pelicula->getAnyo();
            echo '<br>';
            echo 'Género Pelicula: ' . $pelicula->getGenero()->getTipo();
            echo '<br>';
            echo 'Duración Pelicula: ' . $pelicula->getDuracion();
            echo '<br>';
            echo 'Sinopsis Pelicula: ' . $pelicula->getSinopsis();
            echo '<br>';

            // Recuperamos los directores asociados a la pelicula
            $array = corregirArrayStdClass($pelicula->getDirectores()->arrayArtistas);

            echo '<br>';
            echo 'Directores: ';

            //Recorremos el array
            foreach ($array as $valor) {
                //Usamos la funcion para mostrar el nombre del artista
                echo $valor->mostrarNombreArtista();

                echo ', ';
            }

            echo '<br>';
            echo 'Guionistas: ';

            // Recuperamos los guionistas asociados a la pelicula
            $array = corregirArrayStdClass($pelicula->getGuionistas()->arrayArtistas);

            //Recorremos el array
            foreach ($array as $valor) {
                //Usamos la funcion para mostrar el nombre del artista
                echo $valor->mostrarNombreArtista();

                echo ', ';
            }

            echo '<br>';
            echo 'Actores: ';

            // Recuperamos los actores asociados a la pelicula
            $array = corregirArrayStdClass($pelicula->getActores()->arrayArtistas);

            //Recorremos el array
            foreach ($array as $valor) {
                //Usamos la funcion para mostrar el nombre del artista
                echo $valor->mostrarNombreArtista();

                echo ', ';
            }

            echo '<br>';
            echo '----------------------------------------------------------';
            echo '<br>';
        }
        ?>
    </body>
</html>